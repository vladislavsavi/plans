import React from 'react';
import { CheckBox, Icon, Divider } from 'react-native-elements'
import { View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { AsyncStorage } from 'react-native';
import * as appActions from '../store/actions.js';

class TodoItem extends React.Component{
    mark = async () => {
       await this.props.mark(this.props.id);
       const data = JSON.stringify(this.props.todos);
       AsyncStorage.setItem('MyToDo1488', data);  
    }

    deleteToDo = async () => {
       await this.props.delete(this.props.id);
       const data = JSON.stringify(this.props.todos);
       AsyncStorage.setItem('MyToDo1488', data);  
    }

    render(){
        const {text, completed} = this.props;
        return (
            <View style={{display: "flex", flexDirection: "row", borderBottomWidth: 1,
             borderBottomColor: '#eee'}}>
                <CheckBox
                    iconType='evilicon'
                    checkedIcon='check'
                    uncheckedIcon='minus'
                    title={text}
                    checked={completed}
                    containerStyle={{width: '80%', backgroundColor: '#fff', borderColor: '#fff'}}
                    onPress={this.mark}
                    textStyle={{color: completed ? '#9ea0a3' : '#2d2c2c',
                     textDecorationLine: completed ? 'line-through' : 'none'}}
                />
                <TouchableOpacity 
                style={{flexDirection: 'row',
                 display: 'flex',
                 justifyContent: "center",
                 alignContent: "center"}}
                 >
                    <Icon name='trash' type='evilicon'
                    size={30} underlayColor='#fff' onPress={this.deleteToDo} />
                </TouchableOpacity>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        todos: state
    }
}

  const MapDispatchToProps = (dispatch) => {
    return {
      delete: (id) => {
         dispatch(appActions.deleteToDo(id))
      },
      mark: (id) => {
        dispatch(appActions.markToDo(id))
      }
    }
  }
  
  export default connect(mapStateToProps, MapDispatchToProps)(TodoItem);