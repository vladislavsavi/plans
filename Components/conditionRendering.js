import React from 'react';
import ToDoItem from './ToDoItemComponent.js';
import { View, ScrollView, Text } from 'react-native';

export default ({todos}) => {
    if(todos.size > 0){
        const toDoList =  todos.map((todo, index) => {
            return(
                <ToDoItem
                    key={index}
                    text={todo.text}
                    completed={todo.completed} 
                    id={index} 
                />
            ) 
        })
        return (
            <ScrollView>
                 { toDoList }
            </ScrollView>
        );
    }
    else{
        return(
            <View style=
            {{
                flex: 1,
                flexDirection: "row",
                justifyContent: "center",
            }}>
                <Text style={{alignSelf: "center"}}>Please add your first to-do</Text>
            </View>
        );
    }
}