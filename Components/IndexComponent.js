import React from 'react';
import { Header, Icon } from 'react-native-elements';
import { View } from 'react-native';
import { connect } from 'react-redux';
import Dialog from "react-native-dialog";
import * as appActions from '../store/actions.js';
import ConditionRendering from './conditionRendering.js';
import { AsyncStorage } from 'react-native';

class IndexComponent extends React.Component {
  state = {
    dialogVisible: false,
    dialogValue: ''
  };

  showDialog = () => {
    this.setState({ dialogVisible: true });
  };
 
  handleCancel = () => {
    this.setState({ dialogVisible: false, dialogValue: '' });
  };

  getData = async () => {
    await AsyncStorage.getItem('MyToDo1488', (err, result) => {
      let data = JSON.parse(result);
      this.props.getData(data);
    });
  }

  handleSave = async () => {
    let id = this.props.todos.length;
    await this.props.addToDo( {
      text: this.state.dialogValue,
       completed: false,
        id
    })
    const data = JSON.stringify(this.props.todos);
    await AsyncStorage.setItem('MyToDo1488', data);  
    this.setState({ dialogVisible: false, dialogValue: '' });
  };

  componentDidMount(){
    this.getData();
  }

  render(){
    return(
      <View style={{flex: 1}}>

        <Header
          centerComponent={{ text: 'You Plans', style: { color: '#fff', fontSize: 20 } }}
          rightComponent = {<Icon name='note-add'
            onPress={this.showDialog} color='#fff' size={30} iconStyle={{marginTop: 30}}/>
          }
        />

        <ConditionRendering todos={this.props.todos}/>

        <Dialog.Container visible={this.state.dialogVisible}>
          <Dialog.Title>Add you plan</Dialog.Title>
          <Dialog.Input value={this.state.dialogValue} placeholder='Enter description' 
            onChangeText={(text)=>{this.setState({dialogValue: text})}}/>
          <Dialog.Button label="Cancel" onPress={this.handleCancel} />
          <Dialog.Button label="Ok" onPress={this.handleSave}/>
        </Dialog.Container>
      </View>
      
    );
  }
}

const MapStateToProps = (state) => {
  return {
    todos: state
  }
}

const MapDispatchToProps = (dispatch) => {
  return {
    addToDo: (todo) => {
       dispatch(appActions.addToDo(todo))
    },
    getData: (state) => {
      dispatch(appActions.getState(state))
    }
  }
}

export default connect(MapStateToProps, MapDispatchToProps)(IndexComponent);