import React from 'react';
import { Provider  } from 'react-redux';
import { createStore  } from 'redux';
import IndexComponent from './Components/IndexComponent.js';
import reduce from './store/reduce.js';

const store = createStore(reduce);


export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <IndexComponent/>
      </Provider>
    );
  }
}
