export const addToDo = (todo) => {
    return {
        type: 'ADD_TODO',
        todo
    }
}

export const deleteToDo = (id) => {
    return {
        type: 'DELETE_TODO',
        id
    }
}

export const markToDo = (id) => {
    return {
        type: 'MARK_TODO',
        id
    }
}

export const getState = (state) => {
    return {
        type: 'GET_STATE',
        state
    }
}