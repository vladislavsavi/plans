import initialState from "./state.js";
import { List } from 'immutable';

export default ( state = initialState, action ) => {
    switch (action.type) {

        case 'ADD_TODO':
        return state.push(action.todo)
        break;

        case 'DELETE_TODO':
        return state.delete(action.id);
        break;

        case 'MARK_TODO':
        return state.update(action.id, val => {
            return {
               ... val,
               completed: !val.completed
            }
        })

        case 'GET_STATE':
        return List(action.state);
        break;

        break;

        default: 
        return state;
        break;
    }
}